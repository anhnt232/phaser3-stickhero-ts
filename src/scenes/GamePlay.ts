import 'phaser';
import CONFIG from "../../config";

export default class GamePlay extends Phaser.Scene{
    score : number;
    coinGain : number;
    text : Phaser.GameObjects.Text;
    mainPlatform: integer;
    platforms: Array<Phaser.GameObjects.Sprite>;
    coin : Phaser.GameObjects.Sprite;
    coinTrapped : boolean;
    gameMode: integer;
    player : Phaser.GameObjects.Sprite;
    pole : Phaser.GameObjects.Sprite;
    growTween : Phaser.Tweens.Tween;
    walkTween : Phaser.Tweens.Tween;

    gameLevel: number;
    bestGameLevel: number;
    bestScore: number;
    gameLiveCounter: number;

    gameOptions : any;
    logo: Phaser.GameObjects.Sprite;
    scoreText: Phaser.GameObjects.BitmapText;
    bestScoreText: Phaser.GameObjects.BitmapText;

    constructor(){
        super("GamePlay");
        this.score = 0;
        this.gameOptions = {
            growTime: CONFIG.growTimeEasy,
            platformGapRange: CONFIG.platformGapRangeEasy,
            platformWidthRange: CONFIG.platformWidthRangeEasy,
            platformHeight: CONFIG.platformHeight,
            playerWidth: CONFIG.playerWidth,
            playerHeight: CONFIG.playerHeight,
            poleWidth: CONFIG.poleWidth,
            rotateTime: CONFIG.rotateTime,
            walkTime: CONFIG.walkTime,
            fallTime: CONFIG.fallTime,
            scrollTime: CONFIG.scrollTime
        }
    }
    preload(){
        this.load.image("tile", "assets/sprites/tile.png");
        this.load.image("coin", "assets/sprites/coin.png");
        this.load.image("player", "assets/sprites/player.png");

    }
    create(){
        this.gameLiveCounter = localStorage.getItem(CONFIG.localStorageGameLiveCounterName) == null ? CONFIG.GAME_LIVE_MAX : Number(localStorage.getItem(CONFIG.localStorageGameLiveCounterName));

        this.gameLevel = localStorage.getItem(CONFIG.localStorageGameLevelName) == null ? CONFIG.LEVEL_EASY : Number(localStorage.getItem(CONFIG.localStorageGameLevelName));

        this.bestGameLevel = localStorage.getItem(CONFIG.localStorageBestGameLevelName) == null ? CONFIG.LEVEL_EASY : Number(localStorage.getItem(CONFIG.localStorageBestGameLevelName));

        this.coinGain = 1;

        this.bestScore = localStorage.getItem(CONFIG.localStorageBestScoreName) == null ? 0 : Number(localStorage.getItem(CONFIG.localStorageBestScoreName));

        this.score = localStorage.getItem(CONFIG.localStorageLastScoreName) == null ? 0 : Number(localStorage.getItem(CONFIG.localStorageLastScoreName));

        this.addCoin();
        this.addPlatforms();
        this.addPlayer();
        this.addPole();
        this.input.on("pointerdown", this.grow, this);
        this.input.on("pointerup", this.stop, this);

        let x = 150
        let y = 10
        var scorelabels = this.add.sprite(x, y, "scorelabels");
        scorelabels.setOrigin(0,0);
        scorelabels.setScale(0.8);
        var scorepanel = this.add.sprite(x, y+30, "scorepanel");
        scorepanel.setOrigin(0,0);
        scorepanel.setScale(0.8);
        this.scoreText = this.add.bitmapText( x+15, y+50, "font", "0", 50);
        this.scoreText.setOrigin(0, 0);
        this.bestScoreText = this.add.bitmapText( x+290, y+50, "font", this.bestScore.toString(), 50);
        this.bestScoreText.setOrigin(0, 0);
    }
    initGameOption(){
        if(this.gameLevel === CONFIG.LEVEL_EASY){
            this.gameOptions.growTime = CONFIG.growTimeEasy;
            this.gameOptions.platformGapRange = CONFIG.platformGapRangeEasy;
            this.gameOptions.platformWidthRange = CONFIG.platformWidthRangeEasy;
        }
        if(this.gameLevel === CONFIG.LEVEL_MEDIUM){
            this.gameOptions.growTime = CONFIG.growTimeMedium;
            this.gameOptions.platformGapRange = CONFIG.platformGapRangeMedium;
            this.gameOptions.platformWidthRange = CONFIG.platformWidthRangeMedium;
        }
        if(this.gameLevel === CONFIG.LEVEL_HARD){
            this.gameOptions.growTime = CONFIG.growTimeHard;
            this.gameOptions.platformGapRange = CONFIG.platformGapRangeHard;
            this.gameOptions.platformWidthRange = CONFIG.platformWidthRangeHard;
        }
        if(this.gameLevel === CONFIG.LEVEL_VERY_HARD){
            this.gameOptions.growTime = CONFIG.growTimeVeryHard;
            this.gameOptions.platformGapRange = CONFIG.platformGapRangeVeryHard;
            this.gameOptions.platformWidthRange = CONFIG.platformWidthRangeVeryHard;
        }
        if(this.gameLevel === CONFIG.LEVEL_EXTRA_HARD){
            this.gameOptions.growTime = CONFIG.growTimeExtraHard;
            this.gameOptions.platformGapRange = CONFIG.platformGapRangeExtraHard;
            this.gameOptions.platformWidthRange = CONFIG.platformWidthRangeExtraHard;
        }
    }
    addPlatforms(){
        this.initGameOption();
        this.mainPlatform = 0;
        this.platforms = [];
        this.platforms.push(this.addPlatform(0));
        this.platforms.push(this.addPlatform(Number(this.scale.width)));
        this.tweenPlatform();
    }
    addPlatform(posX: number){
        let platform = this.add.sprite(posX, Number(this.scale.height) - this.gameOptions.platformHeight, "tile");
        platform.displayWidth = (this.gameOptions.platformWidthRange[0] + this.gameOptions.platformWidthRange[1]) / 2;
        platform.displayHeight = this.gameOptions.platformHeight;
        platform.alpha = 0.7;
        platform.setOrigin(0, 0);
        return platform
    }
    addCoin(){
        this.coin = this.add.sprite(0, Number(this.scale.height) - this.gameOptions.platformHeight + this.gameOptions.playerHeight / 2, "coin");
        this.coin.visible = false;
    }
    placeCoin(){
        let x1 = this.platforms[this.mainPlatform].getBounds().right;
        let x2 = this.platforms[1 - this.mainPlatform ].getBounds().left;
        let offset = 20;
        let coinPos = Phaser.Math.Between(x1 + offset, x2 - offset);
        function inRange(x:number, min:number, max:number) {
            return ((x-min)*(x-max) <= 0);
        };
        let p2 = x1 + (x2 - x1)/2;
        let p4 = p2 + (x2 - p2)/2;
        let p8 = p4 + (x2 - p4)/2;
       
        if(inRange(coinPos, p2, p4)|| inRange(coinPos, p2-(p4-p2), p2)){
            this.coinGain = 2;
        }
        if(inRange(coinPos, p4, p8) || inRange(coinPos, x1 + x2-p8, p2-(p4-p2))){
            this.coinGain = 3;
        }
        if(inRange(coinPos, p8, x2) || inRange(coinPos, x1, x1 + x2-p8)){
            this.coinGain = 4;
        }
        this.coin.x = coinPos;
        this.coin.visible = true;
        this.coinTrapped = false;
        // console.log(this.coinGain)
    }
    tweenPlatform(){
        this.initGameOption();
        let destination = this.platforms[this.mainPlatform].displayWidth + Phaser.Math.Between(this.gameOptions.platformGapRange[0], 
            this.gameOptions.platformGapRange[1]);
        let size = Phaser.Math.Between(this.gameOptions.platformWidthRange[0], this.gameOptions.platformWidthRange[1]);
        this.tweens.add({
            targets: [this.platforms[1 - this.mainPlatform]],
            x: destination,
            displayWidth: size,
            duration: this.gameOptions.scrollTime,
            callbackScope: this,
            onComplete: function(){
                this.gameMode = CONFIG.WAITING;
                this.placeCoin();
            }
        })
    }
    addPlayer(){
        this.player = this.add.sprite(this.platforms[this.mainPlatform].displayWidth - this.gameOptions.poleWidth, 
            Number(this.scale.height) - this.gameOptions.platformHeight, "player");
        this.player.setOrigin(1, 1)
    }
    addPole(){
        this.pole = this.add.sprite(this.platforms[this.mainPlatform].displayWidth, 
            Number(this.scale.height) - this.gameOptions.platformHeight, "tile");
        this.pole.setOrigin(1, 1);
        this.pole.displayWidth = this.gameOptions.poleWidth;
        this.pole.displayHeight = this.gameOptions.playerHeight / 4;
    }
    grow(){
        if(this.gameMode == CONFIG.WAITING){
            this.gameMode = CONFIG.GROWING;
            this.growTween = this.tweens.add({
                targets: [this.pole],
                displayHeight: this.gameOptions.platformGapRange[1] + this.gameOptions.platformWidthRange[1],
                duration: this.gameOptions.growTime
            });
        }
        if(this.gameMode == CONFIG.WALKING){
            if(this.player.flipY){
                this.player.flipY = false;
                this.player.y = Number(this.scale.height) - this.gameOptions.platformHeight;
            }
            else{
                this.player.flipY = true;
                this.player.y = Number(this.scale.height) - this.gameOptions.platformHeight + this.gameOptions.playerHeight - this.gameOptions.poleWidth;
                let playerBound = this.player.getBounds();
                let platformBound = this.platforms[1 - this.mainPlatform].getBounds();
                if(Phaser.Geom.Rectangle.Intersection(playerBound, platformBound).width != 0){
                    this.player.flipY = false;
                    this.player.y = Number(this.scale.height) - this.gameOptions.platformHeight;
                }
            }
        }
    }
    stop(){
        if(this.gameMode == CONFIG.GROWING){
            this.gameMode = CONFIG.IDLE;
            this.growTween.stop();
            if(this.pole.displayHeight > this.platforms[1 - this.mainPlatform].x - this.pole.x){
                this.tweens.add({
                    targets: [this.pole],
                    angle: 90,
                    duration: this.gameOptions.rotateTime,
                    ease: "Bounce.easeOut",
                    callbackScope: this,
                    onComplete: function(){
                        this.gameMode = CONFIG.WALKING;
                        if(this.pole.displayHeight < this.platforms[1 - this.mainPlatform].x + this.platforms[1 - this.mainPlatform].displayWidth - this.pole.x){
                            this.walkTween = this.tweens.add({
                                targets: [this.player],
                                x: this.platforms[1 - this.mainPlatform].x + this.platforms[1 - this.mainPlatform].displayWidth - this.pole.displayWidth,
                                duration: this.gameOptions.walkTime * this.pole.displayHeight,
                                callbackScope: this,
                                onComplete: function(){
                                    this.coin.visible = false;
                                    this.tweens.add({
                                        targets: [this.player, this.pole, this.platforms[1 - this.mainPlatform], this.platforms[this.mainPlatform]],
                                        props: {
                                            x: {
                                                value: "-= " +  this.platforms[1 - this.mainPlatform].x
                                            }
                                        },
                                        duration: this.gameOptions.scrollTime,
                                        callbackScope: this,
                                        onComplete: function(){
                                            this.prepareNextMove();
                                        }
                                    })
                                }
                            })
                        }
                        else{
                            this.platformTooLong();
                        }
                    }
                })
            }
            else{
                this.platformTooShort();
            }
        }
    }
    platformTooLong(){
        this.walkTween = this.tweens.add({
            targets: [this.player],
            x: this.pole.x + this.pole.displayHeight + this.player.displayWidth,
            duration: this.gameOptions.walkTime * this.pole.displayHeight,
            callbackScope: this,
            onComplete: function(){
                this.fallAndDie();
            }
        })
    }
    platformTooShort(){
        this.tweens.add({
            targets: [this.pole],
            angle: 90,
            duration: this.gameOptions.rotateTime,
            ease: "Cubic.easeIn",
            callbackScope: this,
            onComplete: function(){
                this.gameMode = CONFIG.WALKING;
                this.tweens.add({
                    targets: [this.player],
                    x: this.pole.x + this.pole.displayHeight,
                    duration: this.gameOptions.walkTime * this.pole.displayHeight,
                    callbackScope: this,
                    onComplete: function(){
                        this.tweens.add({
                            targets: [this.pole],
                            angle: 180,
                            duration: this.gameOptions.rotateTime,
                            ease: "Cubic.easeIn"
                        })

                        this.fallAndDie();
                    }
                })
            }
        })
    }
    fallAndDie(){
        this.gameMode = CONFIG.IDLE;
        this.tweens.add({
            targets: [this.player],
            y: Number(this.scale.height) + this.player.displayHeight * 2,
            duration: this.gameOptions.fallTime,
            ease: "Cubic.easeIn",
            callbackScope: this,
            onComplete: function(){
                this.shakeAndRestart();
            }
        })
    }
    prepareNextMove(){
        this.gameMode = CONFIG.IDLE;
        this.platforms[this.mainPlatform].x = Number(this.scale.width);
        this.mainPlatform = 1 - this.mainPlatform;
        this.tweenPlatform();
        this.pole.angle = 0;
        this.pole.x = this.platforms[this.mainPlatform].displayWidth;
        this.pole.displayHeight = this.gameOptions.poleWidth;
        if(this.coinTrapped){
            this.score = this.score + 1 + this.coinGain;
        }else{
            this.score = this.score + 1;
        }
        
        if(this.score > this.bestScore){
            this.bestScore = this.score;
            localStorage.setItem(CONFIG.localStorageBestScoreName, this.bestScore.toString());
            this.bestScoreText.setText(this.bestScore.toString());
        }
        this.scoreText.setText(this.score.toString());

        if(this.score >= CONFIG.LEVEL_EASY_SCORE_THESHOLD)
        {
            this.gameLevel = CONFIG.LEVEL_EASY;
        }
        if(this.score >= CONFIG.LEVEL_MEDIUM_SCORE_THESHOLD)
        {
            this.gameLevel = CONFIG.LEVEL_MEDIUM;
        }
        if(this.score >= CONFIG.LEVEL_HARD_SCORE_THESHOLD)
        {
            this.gameLevel = CONFIG.LEVEL_HARD;
        }
        if(this.score >= CONFIG.LEVEL_VERY_HARD_SCORE_THESHOLD)
        {
            this.gameLevel = CONFIG.LEVEL_VERY_HARD;
        }
        if(this.score >= CONFIG.LEVEL_EXTRA_HARD_SCORE_THESHOLD)
        {
            this.gameLevel = CONFIG.LEVEL_EXTRA_HARD;
        }

        localStorage.setItem(CONFIG.localStorageGameLevelName, this.gameLevel.toString());

        if(this.gameLevel > this.bestGameLevel){
            this.bestGameLevel = this.gameLevel;
            localStorage.setItem(CONFIG.localStorageBestGameLevelName, this.bestGameLevel.toString());
        }
    }
    shakeAndRestart(){
        this.cameras.main.shake(800, 0.01);
        this.time.addEvent({
            delay: 2000,
            callbackScope: this,
            callback: function(){
                localStorage.setItem(CONFIG.localStorageLastScoreName, this.score.toString());
                this.gameLiveCounter--;
                // console.log("gameLiveCounter", gameLiveCounter);
                if(this.gameLiveCounter === 0) {
                    this.scene.start("GameOver");
                    this.gameLiveCounter = CONFIG.GAME_LIVE_MAX;
                    localStorage.setItem(CONFIG.localStorageGameLiveCounterName, this.gameLiveCounter.toString());
                    this.gameLevel = CONFIG.LEVEL_EASY;
                    localStorage.setItem(CONFIG.localStorageGameLevelName, this.gameLevel.toString());
                    this.score = 0;
                    localStorage.setItem(CONFIG.localStorageLastScoreName, this.score.toString());
                }
                else{
                    this.scene.start("GamePlay");
                    localStorage.setItem(CONFIG.localStorageGameLiveCounterName, this.gameLiveCounter.toString());
                    localStorage.setItem(CONFIG.localStorageGameLevelName, this.gameLevel.toString());
                    localStorage.setItem(CONFIG.localStorageLastScoreName, this.score.toString());
                }
                
            }
        })
    }

    update(){
        if(this.player.flipY){
            let playerBound = this.player.getBounds();
            let coinBound = this.coin.getBounds();
            let platformBound = this.platforms[1 - this.mainPlatform].getBounds();
            if(Phaser.Geom.Rectangle.Intersection(playerBound, platformBound).width != 0){
                this.walkTween.stop();
                this.gameMode = CONFIG.IDLE;
                this.shakeAndRestart();
            }
            if(this.coin.visible && Phaser.Geom.Rectangle.Intersection(playerBound, coinBound).width != 0){
                this.coin.visible = false;
                this.coinTrapped = true;
            }
        }
    }
};
